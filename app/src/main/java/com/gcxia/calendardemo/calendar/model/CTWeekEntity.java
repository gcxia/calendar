package com.gcxia.calendardemo.calendar.model;

import java.util.ArrayList;
/**
 * @author gcxia
 * @date 2017/5/24
 * @description
 */
public class CTWeekEntity {

    public ArrayList<CTDayEntity> _days = new ArrayList<CTDayEntity>();

    public boolean isCurrentMonth() {
        return _days.get(0)._isCurrentMonth || _days.get(6)._isCurrentMonth;
    }
}
