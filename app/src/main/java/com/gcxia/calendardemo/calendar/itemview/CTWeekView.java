package com.gcxia.calendardemo.calendar.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.gcxia.calendardemo.calendar.listener.DayViewClickListener;
import com.gcxia.calendardemo.calendar.model.CTDayEntity;
import com.gcxia.calendardemo.calendar.model.CTWeekEntity;

/**
 * @author gcxia
 * @date 2017/5/24
 * @description
 */
public class CTWeekView extends LinearLayout {

    private DayViewClickListener listener;

    public CTWeekView(Context context) {
        super(context);
        init(context);
    }

    public CTWeekView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CTWeekView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        for (int i = 0; i < 7; i++) {
            addView(createCTDayView(context));
        }
    }

    public void setWeekData(CTWeekEntity week) {
        for (int i = 0; i < week._days.size(); i++) {
            CTDayEntity day = week._days.get(i);
            CTBaseDayView dayView = (CTBaseDayView) getChildAt(i);
            dayView.setDayData(day);
            dayView.setListener(listener);
        }
    }

    public void setDayListener(DayViewClickListener listener) {
        this.listener = listener;
    }

    public CTBaseDayView createCTDayView(Context context){
        CTBaseDayView ctDayView = new CTDayView(context);
        LayoutParams params = new LayoutParams(0, LayoutParams.WRAP_CONTENT, 1.0f);
        ctDayView.setLayoutParams(params);
        return ctDayView;
    }

}
