package com.gcxia.calendardemo.calendar.itemview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.gcxia.calendardemo.R;
import com.gcxia.calendardemo.calendar.model.CTDayEntity;

/**
 * @author gcxia
 * @date 2017/5/26
 * @description
 */

public class CTCalendarFrameLayout extends FrameLayout {

    private static final int[] STATE_TODAY = {R.attr.state_today};
    private static final int[] STATE_SELECT = {R.attr.state_select};
    private final Paint _paint = new Paint();

    private CTDayEntity dayEntity;

    public CTCalendarFrameLayout(Context context) {
        super(context);
        init();
    }

    public CTCalendarFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CTCalendarFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setDayEntity(CTDayEntity dayEntity) {
        this.dayEntity = dayEntity;
    }

    private void init() {
        _paint.setColor(getResources().getColor(R.color.white_alp_30));
        _paint.setStyle(Paint.Style.FILL);
        _paint.setAntiAlias(true);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (dayEntity != null && dayEntity._isIntervalTime) {
            int x = getWidth();
            int y = getHeight();
            if (dayEntity._isDepart) {
                canvas.drawRect(x / 2, 0, x, y, _paint);
            } else if (dayEntity._isReturn) {
                canvas.drawRect(0, 0, x / 2, y, _paint);
            } else {
                canvas.drawRect(0, 0, x, y, _paint);
            }
        }
        super.onDraw(canvas);
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        final int[] drawableState = super.onCreateDrawableState(extraSpace + 2);
        if (dayEntity != null) {
            if (dayEntity._isToday)
                mergeDrawableStates(drawableState, STATE_TODAY);
            if (dayEntity._isSelected || dayEntity._isDepart || dayEntity._isReturn)
                mergeDrawableStates(drawableState, STATE_SELECT);
        }
        return drawableState;
    }
}
