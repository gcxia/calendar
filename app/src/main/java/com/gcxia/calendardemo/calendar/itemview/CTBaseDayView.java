package com.gcxia.calendardemo.calendar.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.gcxia.calendardemo.calendar.listener.DayViewClickListener;
import com.gcxia.calendardemo.calendar.model.CTDayEntity;

/**
 * @author gcxia
 * @date 2017/5/24
 * @description
 */

public abstract class CTBaseDayView extends FrameLayout {

    public CTBaseDayView(Context context) {
        super(context);
        init(context);
    }

    public CTBaseDayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CTBaseDayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    private void init(Context context) {
        View.inflate(context, getContentView(), this);
        findView();
    }

    public abstract int getContentView();

    public abstract void findView();

    public abstract void setDayData(CTDayEntity day);

    public abstract void setListener(DayViewClickListener listener);
}
