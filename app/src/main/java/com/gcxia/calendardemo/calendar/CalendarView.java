package com.gcxia.calendardemo.calendar;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListView;

import com.gcxia.calendardemo.calendar.adapter.CalendarAdapter;
import com.gcxia.calendardemo.calendar.listener.CalendarListener;
import com.gcxia.calendardemo.calendar.listener.DayViewClickListener;
import com.gcxia.calendardemo.calendar.model.CTDayEntity;
import com.gcxia.calendardemo.calendar.model.CalendarData;

/**
 * @author gcxia
 * @date 2017/5/24
 * @description
 */

public class CalendarView extends ListView implements DayViewClickListener {
    private CalendarAdapter calendarAdapter;
    private CalendarData calendarData;
    private CalendarListener calendarListener;

    public CalendarView(Context context) {
        super(context);
        initView();
    }

    public CalendarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public CalendarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        setDivider(null);
        setCacheColorHint(getResources().getColor(android.R.color.transparent));
        setVerticalScrollBarEnabled(false);

        calendarAdapter = new CalendarAdapter(CalendarData._months);
        calendarAdapter.setDateListener(this);
        setAdapter(calendarAdapter);
    }

    public void setCalendarData(CalendarData calendarData) {
        this.calendarData = calendarData;
    }

    public void setCalendarListener(CalendarListener calendarListener) {
        this.calendarListener = calendarListener;
    }

    public CalendarAdapter getCalendarAdapter() {
        return calendarAdapter;
    }

    public void notifyDataSetChanged() {
        calendarAdapter.notifyDataSetChanged();
    }

    public void setSelectType(int type) {
        calendarData.type = type;
    }

    public void setUnableData() {
        calendarData.setUnableData();
    }

    @Override
    public void onClickItemDate(View dayView, CTDayEntity dayEntity) {
        if (calendarData.type == CalendarData.RADIO_DATE) {
            if (dayEntity == calendarData.selectDate) {
                return;
            }
            calendarData.changeOneWayData(dayEntity);
            notifyDataSetChanged();
            calendarListener.onSelectedOneWayDate(dayView, dayEntity);
        } else if (calendarData.type == CalendarData.DEPART_DATE) {
            if (dayEntity == calendarData.departDate) {
                return;
            }
            calendarData.changeDepartData(dayEntity);
            notifyDataSetChanged();
            calendarListener.onSelectedDepartDate(dayView, dayEntity);
        } else if (calendarData.type == CalendarData.RETURN_DATE) {
            if (dayEntity == calendarData.returnDate) {
                return;
            }
            calendarData.changeReturnData(dayEntity);
            notifyDataSetChanged();
            calendarListener.onSelectedReturnDate(dayView, dayEntity);
        }
    }
}
