package com.gcxia.calendardemo.calendar.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gcxia.calendardemo.R;
import com.gcxia.calendardemo.calendar.listener.DayViewClickListener;
import com.gcxia.calendardemo.calendar.model.CTDayEntity;

/**
 * @author gcxia
 * @date 2017/5/25
 * @description
 */

public class CTDayView extends CTBaseDayView implements View.OnClickListener {

    private TextView tv_day;
    private CTCalendarFrameLayout fl_day_root;
    private View v_left;
    private View v_right;
    private LinearLayout ll_interval_bg;
    private DayViewClickListener listener;
    private CTDayEntity day;

    public CTDayView(Context context) {
        super(context);
    }

    public CTDayView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CTDayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public int getContentView() {
        return R.layout.item_calendar_day;
    }

    @Override
    public void findView() {
        setBackgroundResource(R.drawable.selector_color_trans_pressed);
        setOnClickListener(this);
        fl_day_root = (CTCalendarFrameLayout) findViewById(R.id.fl_day_root);
        tv_day = (TextView) findViewById(R.id.tv_day);
        ll_interval_bg = (LinearLayout) findViewById(R.id.ll_interval_bg);
        v_left = findViewById(R.id.v_left);
        v_right = findViewById(R.id.v_right);
    }

    @Override
    public void setDayData(CTDayEntity day) {
        this.day = day;
        setVisibility(day._isCurrentMonth ? View.VISIBLE : View.INVISIBLE);
        tv_day.setText(String.valueOf(day._date.getDayOfMonth()));
        if(day._unable){
            tv_day.setTextColor(getResources().getColor(R.color.white_alp_50));
        }else if(day._isSelected||day._isReturn||day._isDepart){
            tv_day.setTextColor(getResources().getColor(R.color.color_076bb0));
        }else{
            tv_day.setTextColor(getResources().getColor(R.color.color_ffffff));
        }

        fl_day_root.setDayEntity(day);
        fl_day_root.refreshDrawableState();
        setEnabled(!day._unable);
    }

    @Override
    public void onClick(View view) {
        listener.onClickItemDate(this, day);
    }

    public void setListener(DayViewClickListener listener) {
        this.listener = listener;
    }
}
