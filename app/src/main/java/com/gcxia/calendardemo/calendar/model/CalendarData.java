package com.gcxia.calendardemo.calendar.model;

import com.gcxia.calendardemo.DateTimeUtil;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * @author gcxia
 * @date 2017/5/24
 * @description
 */

public class CalendarData {

    public static final int DEPART_DATE = 1;
    public static final int RETURN_DATE = 2;
    public static final int RADIO_DATE = 4;

    public static final int MONTH_COUNT = 12;
    public static ArrayList<CTMonthEntity> _months = new ArrayList<CTMonthEntity>();
    public static HashMap<String, CTDayEntity> _allDays = new HashMap<String, CTDayEntity>();

    public int type = RADIO_DATE;
    public CTDayEntity selectDate;
    public CTDayEntity departDate;
    public CTDayEntity returnDate;
    public DateTime startTime = DateTime.now();
    public DateTime endTime = DateTime.now().plusDays(364);

    /**
     * 设置日历Date中的类型及单选日期或往返日期
     *
     * @param type
     * @param dayEntitys
     * @return
     */
    public CalendarData initCalendarData(int type, CTDayEntity... dayEntitys) {
        this.type = type;
        if (dayEntitys != null) {
            if (type == RADIO_DATE) {
                if (dayEntitys.length > 0)
                    selectDate = dayEntitys[0];
            } else {
                if (dayEntitys.length >= 2) {
                    departDate = dayEntitys[0];
                    returnDate = dayEntitys[1];
                } else if (dayEntitys.length == 1) {
                    departDate = dayEntitys[0];
                }
            }
        }
        return this;
    }

    /**
     * 设置日历起始结束可选日期范围
     *
     * @param startTime
     * @param endTime
     * @return
     */
    public CalendarData setLimitDate(DateTime startTime, DateTime endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
        return this;
    }

    public void createMonths(int mothonCount) {
        if (mothonCount <= 0) {
            mothonCount = MONTH_COUNT;
        }

        DateTime dateTime = DateTime.now();
        CalendarData._months.clear();
        CalendarData._allDays.clear();
        for (int i = 0; i < mothonCount; i++) {
            CalendarData._months.add(new CTMonthEntity(type).createMonthData(dateTime.withDayOfMonth(1), startTime, endTime));
            dateTime = dateTime.plusMonths(1);
        }
    }

    /**
     * 只有往返程才会调用这个方法，设置哪些日期不可点
     */
    public void setUnableData() {
        DateTime start = startTime;
        if (type == RETURN_DATE) {
            start = departDate == null ? startTime : departDate._date;
        }

        for (Map.Entry<String, CTDayEntity> entry : _allDays.entrySet()) {
            CTDayEntity dayEntity = entry.getValue();
            if (DateTimeUtil.compareWithField(dayEntity._date, start, Calendar.DAY_OF_MONTH) < 0 || DateTimeUtil.compareWithField(dayEntity._date, endTime, Calendar.DAY_OF_MONTH) > 0) {
                dayEntity._unable = true;
            } else {
                dayEntity._unable = false;
            }

            if (departDate != null && returnDate != null) {
                if (departDate == returnDate) {
                    dayEntity._isIntervalTime = false;
                } else {
                    if (DateTimeUtil.compareWithField(dayEntity._date, departDate._date, Calendar.DAY_OF_MONTH) >= 0 && DateTimeUtil.compareWithField(dayEntity._date, returnDate._date, Calendar.DAY_OF_MONTH) <= 0) {
                        dayEntity._isIntervalTime = true;
                    } else {
                        dayEntity._isIntervalTime = false;
                    }
                }
            } else {
                dayEntity._isIntervalTime = false;
            }
        }
    }

    /**
     * 重置选中日期的状态
     *
     * @param day
     * @param type
     */
    public void resetDayEntity(CTDayEntity day, int type) {
        if (day != null) {
            if ((type & DEPART_DATE) == DEPART_DATE)
                day._isDepart = false;
            if ((type & RETURN_DATE) == RETURN_DATE)
                day._isReturn = false;
            if ((type & RADIO_DATE) == RADIO_DATE)
                day._isSelected = false;
            day._unable = false;
            day._isIntervalTime = false;
        }
    }

    public void changeOneWayData(CTDayEntity dayEntity) {
        resetDayEntity(selectDate, RADIO_DATE);
        dayEntity._isSelected = true;
        selectDate = dayEntity;
    }

    public void changeDepartData(CTDayEntity dayEntity) {
        resetDayEntity(departDate, DEPART_DATE);
        dayEntity._isDepart = true;
        departDate = dayEntity;
        if (returnDate != null && DateTimeUtil.compareWithField(dayEntity._date, returnDate._date, Calendar.DAY_OF_MONTH) > 0) {
            resetDayEntity(returnDate, RETURN_DATE);
            returnDate = null;
        }
        setUnableData();
    }

    public void changeReturnData(CTDayEntity dayEntity) {
        resetDayEntity(returnDate, RETURN_DATE);
        dayEntity._isReturn = true;
        returnDate = dayEntity;
        setUnableData();
    }
}
