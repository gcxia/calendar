package com.gcxia.calendardemo.calendar.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.gcxia.calendardemo.calendar.itemview.CTMonthView;
import com.gcxia.calendardemo.calendar.listener.DayViewClickListener;
import com.gcxia.calendardemo.calendar.model.CTMonthEntity;

import java.util.ArrayList;

/**
 * @author gcxia
 * @date 2017/5/24
 * @description
 */

public class CalendarAdapter extends BaseAdapter {

    private ArrayList<CTMonthEntity> _months;
    private DayViewClickListener listener;

    public CalendarAdapter(ArrayList<CTMonthEntity> _months) {
        this._months = _months;
    }

    @Override
    public int getCount() {
        return _months.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public void setDateListener(DayViewClickListener listener){
        this.listener = listener;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        CTMonthView monthView = (CTMonthView) convertView;
        if (monthView == null) {
            monthView = new CTMonthView(viewGroup.getContext());
        }
        monthView.setMonthData(_months.get(i),listener);
        return monthView;
    }
}
