package com.gcxia.calendardemo.calendar.model;

import org.joda.time.DateTime;

/**
 * @author gcxia
 * @date 2017/5/24
 * @description
 */
public class CTDayEntity {

    /**
     * 当天日期
     */
    public DateTime _date;

    /**
     * 选择日期的类型：往返，还是单程
     */
    public int selectType;

    /**
     * 是否是属于当前月份的日期，不是的话不展示
     */
    public boolean _isCurrentMonth;

    /**
     * 是否是今天
     */
    public boolean _isToday;

    /**
     * 单程是否是选中状态
     */
    public boolean _isSelected;

    /**
     * 往返是否去程日期
     */
    public boolean _isDepart;

    /**
     * 往返是否返程日期
     */
    public boolean _isReturn;

    /**
     * 不可点击
     */
    public boolean _unable;

    /**
     * 往返程日期都选了，是否是中间的间隔时间
     */
    public boolean _isIntervalTime;

    /**
     * 是否是所有低价中最低的
     */
    public boolean _isLowestPrice;

    /**
     * 低价价格
     */
    public boolean _lowPrice;

}
