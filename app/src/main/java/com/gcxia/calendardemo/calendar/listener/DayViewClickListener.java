package com.gcxia.calendardemo.calendar.listener;

import android.view.View;

import com.gcxia.calendardemo.calendar.model.CTDayEntity;

/**
 * @author gcxia
 * @date 2017/5/25
 * @description
 */

public interface DayViewClickListener {
    void onClickItemDate(View dayView, CTDayEntity dayEntity);
}
