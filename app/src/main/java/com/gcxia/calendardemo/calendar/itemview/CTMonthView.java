package com.gcxia.calendardemo.calendar.itemview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gcxia.calendardemo.R;
import com.gcxia.calendardemo.calendar.listener.DayViewClickListener;
import com.gcxia.calendardemo.calendar.model.CTMonthEntity;
import com.gcxia.calendardemo.calendar.model.CTWeekEntity;

/**
 * @author gcxia
 * @date 2017/5/24
 * @description
 */
public class CTMonthView extends LinearLayout {

    private TextView _tvTitle;
    private LinearLayout _llWeekContent;

    public CTMonthView(Context context) {
        super(context);
        init(context);
    }

    public CTMonthView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        View view = View.inflate(context, R.layout.view_flight_month, this);
        _tvTitle = (TextView) view.findViewById(R.id.tv_title);
        _llWeekContent = (LinearLayout) view.findViewById(R.id.ll_week_day_content);
    }

    public void setMonthData(CTMonthEntity month, DayViewClickListener listener) {
        _tvTitle.setText(month._firstDate.toString("yyyy年M月"));
        for (int i = 0; i < 6; i++) {
            CTWeekView weekView = (CTWeekView) _llWeekContent.getChildAt(i);
            weekView.setDayListener(listener);
            CTWeekEntity week = month._weeks.get(i);
            if (weekView == null) {
                continue;
            }
            if (week != null && week.isCurrentMonth()) {
                weekView.setVisibility(View.VISIBLE);
                weekView.setWeekData(week);
            } else {
                weekView.setVisibility(View.GONE);
            }
        }
    }
}
