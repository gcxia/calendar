package com.gcxia.calendardemo.calendar.model;

import com.gcxia.calendardemo.DateTimeUtil;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * @author gcxia
 * @date 2017/5/24
 * @description
 */
public class CTMonthEntity {

    public ArrayList<CTWeekEntity> _weeks = new ArrayList<CTWeekEntity>();
    public DateTime _firstDate;
    public int _year;
    public int _month;
    /**
     * 选择日期的类型：往返，还是单程
     */
    public int selectType;

    public CTMonthEntity(int selectType) {
        this.selectType = selectType;
    }

    /**
     * 往返 --- 返程进入，若去程日期不为null，则将去程日期当做startTime
     */
    public CTMonthEntity createMonthData(DateTime dateTime, DateTime startTime, DateTime endTime) {
        _firstDate = dateTime;
        _month = _firstDate.getMonthOfYear();
        _year = _firstDate.getYear();
        DateTime tempDateTime = dateTime.minusDays(dateTime.getDayOfWeek());
        for (int i = 0; i < 6; i++) {
            CTWeekEntity week = new CTWeekEntity();
            _weeks.add(week);
            for (int j = DateTimeConstants.MONDAY; j <= DateTimeConstants.SUNDAY; j++) {
                CTDayEntity dayEntity = new CTDayEntity();
                dayEntity.selectType = selectType;
                dayEntity._date = tempDateTime;
                dayEntity._isToday = DateTimeUtil.compareWithField(tempDateTime, DateTime.now(), Calendar.DAY_OF_MONTH) == 0;
                dayEntity._isCurrentMonth = tempDateTime.getMonthOfYear() == _month;
                if (startTime != null && endTime != null) {
                    if (DateTimeUtil.compareWithField(tempDateTime, startTime, Calendar.DAY_OF_MONTH) < 0 || DateTimeUtil.compareWithField(tempDateTime, endTime, Calendar.DAY_OF_MONTH) > 0) {
                        dayEntity._unable = true;
                    }
                }
                if (dayEntity._isCurrentMonth)
                    CalendarData._allDays.put(tempDateTime.toString("yyyy-MM-dd"), dayEntity);
                week._days.add(dayEntity);
                tempDateTime = tempDateTime.plusDays(1);
            }
        }

        return this;
    }
}
