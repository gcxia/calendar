package com.gcxia.calendardemo.calendar.listener;

import android.view.View;

import com.gcxia.calendardemo.calendar.model.CTDayEntity;

/**
 * @author gcxia
 * @date 2017/5/26
 * @description
 */

public interface CalendarListener {
    void onSelectedDepartDate(View dayView, CTDayEntity dayEntity);

    void onSelectedReturnDate(View dayView, CTDayEntity dayEntity);

    void onSelectedOneWayDate(View dayView, CTDayEntity dayEntity);
}
