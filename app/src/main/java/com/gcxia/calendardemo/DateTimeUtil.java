package com.gcxia.calendardemo;

import org.joda.time.DateTime;

import java.util.Calendar;

/**
 * @author gcxia
 * @date 2017/5/25
 * @description
 */

public class DateTimeUtil {
    /**
     * 指定某个域比较两个日期
     * aDateTime < bDateTime return -1
     * aDateTime = bDateTime return 0
     * aDateTime > bDateTime return 1
     *
     * @return
     */
    public static int compareWithField(DateTime aDateTime, DateTime bDateTime, int Field) {
        if (aDateTime == null && bDateTime != null) {
            return -1;
        }
        if (aDateTime != null && bDateTime == null) {
            return 1;
        }
        if (aDateTime == null && bDateTime == null) {
            return 0;
        }

        long aNumber = DateTimeUtil.getDateTimeNumber(aDateTime, Field);
        long bNumber = DateTimeUtil.getDateTimeNumber(bDateTime, Field);
        if (aNumber < bNumber)
            return -1;
        else if (aNumber > bNumber) {
            return 1;
        } else {
            return 0;
        }
    }

    public static long getDateTimeNumber(DateTime dateTime, int field) {
        long number = 0;
        long multiple = 1;
        switch (field) {
            case Calendar.MILLISECOND:
                number += dateTime.getMillisOfSecond() * multiple;
                multiple = multiple * 1000;
            case Calendar.SECOND:
                number += dateTime.getSecondOfMinute() * multiple;
                multiple = multiple * 100;
            case Calendar.MINUTE:
                number += dateTime.getMinuteOfHour() * multiple;
                multiple = multiple * 100;
            case Calendar.HOUR:
                number += dateTime.getHourOfDay() * multiple;
                multiple = multiple * 100;
            case Calendar.DAY_OF_MONTH:
                number += dateTime.getDayOfMonth() * multiple;
                multiple = multiple * 100;
            case Calendar.MONTH:
                number += dateTime.getMonthOfYear() * multiple;
                multiple = multiple * 100;
            case Calendar.YEAR:
                number += dateTime.getYear() * multiple;
            default:
                break;
        }
        return number;
    }
}
