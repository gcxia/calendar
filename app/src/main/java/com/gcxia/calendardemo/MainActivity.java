package com.gcxia.calendardemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.gcxia.calendardemo.calendar.CalendarView;
import com.gcxia.calendardemo.calendar.listener.CalendarListener;
import com.gcxia.calendardemo.calendar.model.CTDayEntity;
import com.gcxia.calendardemo.calendar.model.CalendarData;

public class MainActivity extends AppCompatActivity implements CalendarListener {

    private CalendarView cvCalenderListView;
    public int type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        pop();
//        calendarData.setUnableData(calendarData.startTime);
        cvCalenderListView = (CalendarView) findViewById(R.id.cv_calender_listview);
        CalendarData calendarData = new CalendarData().initCalendarData(CalendarData.DEPART_DATE, null, null);
        calendarData.createMonths(13);
        cvCalenderListView.setCalendarListener(this);
        cvCalenderListView.setCalendarData(calendarData);
        cvCalenderListView.notifyDataSetChanged();
    }


    public void oneWay(View v) {
        cvCalenderListView.setSelectType(CalendarData.RADIO_DATE);
    }

    public void departBtn(View v) {
        cvCalenderListView.setSelectType(CalendarData.DEPART_DATE);
        cvCalenderListView.setUnableData();
        cvCalenderListView.notifyDataSetChanged();
    }

    public void returnBtn(View v) {
        cvCalenderListView.setSelectType(CalendarData.RETURN_DATE);
        cvCalenderListView.setUnableData();
        cvCalenderListView.notifyDataSetChanged();
    }

    @Override
    public void onSelectedDepartDate(View dayView, CTDayEntity dayEntity) {

    }

    @Override
    public void onSelectedReturnDate(View dayView, CTDayEntity dayEntity) {

    }

    @Override
    public void onSelectedOneWayDate(View dayView, CTDayEntity dayEntity) {

    }
}
